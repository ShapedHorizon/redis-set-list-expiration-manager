FROM golang:1.14.1
WORKDIR /go/src/redis_mngr/
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...

RUN chmod +x ./start.sh
RUN apt-get update
RUN apt -y install netcat \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent

EXPOSE 50051

CMD ./start.sh