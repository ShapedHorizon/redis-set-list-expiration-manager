package main

import (
	"context"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/ShapedHorizon/redis-set-list-expiration-manager/v2/redismngr"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v2"
)

type server struct{}

var mutex sync.Mutex

var noEntry bool

type redisCreds struct {
	Address  string `yaml:"address" envconfig:"REDIS_ADDRESS"`
	Password string `yaml:"password" envconfig:"REDIS_PASS"`
	DB       int    `yaml:"db" envconfig:"REDIS_DB"`
}

var redisClient *redis.Client

func main() {

	noEntry = true

	conf := redisCreds{}

	readConfFile(&conf)
	readEnv(&conf)

	redisClient = redis.NewClient(&redis.Options{
		// Addr: conf.Address,
		Addr:     "redis:6379",
		Password: conf.Password,
		DB:       conf.DB,
	})

	defer redisClient.Close()

	_, err := redisClient.Ping().Result()
	handleErr("Cannot ping redis db", err)

	go checkForExpirationList()
	go checkForExpirationSet()
	go checkForExpirationSortedSet()

	address := ":50051"
	lis, err := net.Listen("tcp", address)
	handleErr("Cannot listen to 0.0.0.0:50051", err)
	s := grpc.NewServer()

	redismngr.RegisterRedisMngrServiceServer(s, &server{})

	log.Println("Starting Listening")

	s.Serve(lis)

}

func (*server) NewListMember(ctx context.Context, request *redismngr.RedisData) (*redismngr.NoMsg, error) {
	dur, err := time.ParseDuration(request.Expires)
	if err != nil {
		return nil, err
	}

	expTime := time.Now().Add(dur).Unix()
	mutex.Lock()
	_, err = redisClient.ZAddCh("redismngr:expiryset:list", &redis.Z{
		Member: request.Key + ":redismngr:" + request.ElementValue,
		Score:  float64(expTime),
	}).Result()
	mutex.Unlock()
	if err != nil {
		return nil, err
	}

	noEntry = false
	return &redismngr.NoMsg{}, nil
}

func (*server) NewSetMember(ctx context.Context, request *redismngr.RedisData) (*redismngr.NoMsg, error) {

	dur, err := time.ParseDuration(request.Expires)
	if err != nil {
		return nil, err
	}

	expTime := time.Now().Add(dur).Unix()
	mutex.Lock()
	_, err = redisClient.ZAddCh("redismngr:expiryset:set", &redis.Z{
		Member: request.Key + ":redismngr:" + request.ElementValue,
		Score:  float64(expTime),
	}).Result()
	mutex.Unlock()
	if err != nil {
		return nil, err
	}

	noEntry = false
	return &redismngr.NoMsg{}, nil
}

func (*server) NewSortedSetMember(ctx context.Context, request *redismngr.RedisData) (*redismngr.NoMsg, error) {
	dur, err := time.ParseDuration(request.Expires)
	if err != nil {
		return nil, err
	}

	expTime := time.Now().Add(dur).Unix()
	mutex.Lock()
	_, err = redisClient.ZAddCh("redismngr:expiryset:sortedset", &redis.Z{
		Member: request.Key + ":redismngr:" + request.ElementValue,
		Score:  float64(expTime),
	}).Result()
	mutex.Unlock()
	if err != nil {
		return nil, err
	}

	noEntry = false
	return &redismngr.NoMsg{}, nil
}

func checkForExpirationSet() {
	firstexpire := time.Now().Unix()
	invalidExpiryFlag := true
	for {
		if noEntry {
			continue
		}
		now := time.Now().Unix()

		if invalidExpiryFlag {
			mutex.Lock()
			exp, err := redisClient.ZRangeWithScores("redismngr:expiryset:set", 0, 0).Result()
			mutex.Unlock()
			if err != nil && err != redis.Nil {
				log.Printf("Error getting redismngr:expiryset:set datas ... %v\n", err)
				continue
			} else if err == redis.Nil || len(exp) == 0 {
				noEntry = true
				continue
			}
			firstexpire = int64(exp[0].Score)
			invalidExpiryFlag = false
		}

		if firstexpire <= now {
			mutex.Lock()
			elem, err := redisClient.ZPopMin("redismngr:expiryset:set", 1).Result()
			mutex.Unlock()
			if err != nil && err != redis.Nil {
				log.Printf("Error getting redismngr:expiryset:set datas ... %v\n", err)
				continue
			} else if err == redis.Nil {
				continue
			}

			if elem[0].Score > float64(now) {
				mutex.Lock()
				exp, err := redisClient.ZRangeWithScores("redismngr:expiryset:set", 0, 0).Result()
				mutex.Unlock()
				if err != nil && err != redis.Nil {
					log.Printf("Error getting redismngr:expiryset:set datas ... %v\n", err)
					continue
				} else if err == redis.Nil {
					continue
				}
				firstexpire = int64(exp[0].Score)
				continue
			}

			key, val := parseKeyValue(elem[0].Member.(string))

			mutex.Lock()
			_, err = redisClient.SRem(key, val).Result()
			mutex.Unlock()
			log.Printf("Removed %s : %s \n", key, val)
			if err != nil {
				// TODO: handle error better
				// E.g. by putting the data back to the set to try to remove it again
				log.Printf("Error removing %s from %s set ... %v\n", val, key, err)
				continue
			}
			invalidExpiryFlag = true

		}
	}
}

func checkForExpirationList() {
	firstexpire := time.Now().Unix()
	invalidExpiryFlag := true
	for {
		if noEntry {
			continue
		}
		now := time.Now().Unix()

		if invalidExpiryFlag {
			mutex.Lock()
			exp, err := redisClient.ZRangeWithScores("redismngr:expiryset:list", 0, 0).Result()
			mutex.Unlock()
			if err != nil && err != redis.Nil {
				log.Printf("Error getting redismngr:expiryset:list datas ... %v\n", err)
				continue
			} else if err == redis.Nil || len(exp) == 0 {
				continue
			}
			firstexpire = int64(exp[0].Score)
			invalidExpiryFlag = false
		}

		if firstexpire <= now {
			mutex.Lock()
			elem, err := redisClient.ZPopMin("redismngr:expiryset:list", 1).Result()
			mutex.Unlock()
			if err != nil && err != redis.Nil {
				log.Printf("Error getting redismngr:expiryset:list datas ... %v\n", err)
				continue
			} else if err == redis.Nil {
				continue
			}

			if elem[0].Score > float64(now) {
				mutex.Lock()
				exp, err := redisClient.ZRangeWithScores("redismngr:expiryset:list", 0, 0).Result()
				mutex.Unlock()
				if err != nil && err != redis.Nil {
					log.Printf("Error getting redismngr:expiryset:list datas ... %v\n", err)
					continue
				} else if err == redis.Nil {
					continue
				}
				firstexpire = int64(exp[0].Score)
				continue
			}

			key, val := parseKeyValue(elem[0].Member.(string))

			mutex.Lock()
			_, err = redisClient.LRem(key, 1, val).Result()
			mutex.Unlock()
			log.Printf("Removed %s : %s \n", key, val)
			if err != nil {
				// TODO: handle error better
				// E.g. by putting the data back to the set to try to remove it again
				log.Printf("Error removing %s from %s set ... %v\n", val, key, err)
				continue
			}
			invalidExpiryFlag = true

		}
	}
}

func checkForExpirationSortedSet() {
	firstexpire := time.Now().Unix()
	invalidExpiryFlag := true
	for {
		if noEntry {
			continue
		}
		now := time.Now().Unix()

		if invalidExpiryFlag {
			mutex.Lock()
			exp, err := redisClient.ZRangeWithScores("redismngr:expiryset:sortedset", 0, 0).Result()
			mutex.Unlock()
			if err != nil && err != redis.Nil {
				log.Printf("Error getting redismngr:expiryset:sortedset datas ... %v\n", err)
				continue
			} else if err == redis.Nil || len(exp) == 0 {
				noEntry = true
				continue
			}
			firstexpire = int64(exp[0].Score)
			invalidExpiryFlag = false
		}

		if firstexpire <= now {
			mutex.Lock()
			elem, err := redisClient.ZPopMin("redismngr:expiryset:sortedset", 1).Result()
			mutex.Unlock()
			if err != nil && err != redis.Nil {
				log.Printf("Error getting redismngr:expiryset:sortedset datas ... %v\n", err)
				continue
			} else if err == redis.Nil {
				continue
			}

			if elem[0].Score > float64(now) {
				mutex.Lock()
				exp, err := redisClient.ZRangeWithScores("redismngr:expiryset:sortedset", 0, 0).Result()
				mutex.Unlock()
				if err != nil && err != redis.Nil {
					log.Printf("Error getting redismngr:expiryset:sortedset datas ... %v\n", err)
					continue
				} else if err == redis.Nil {
					continue
				}
				firstexpire = int64(exp[0].Score)
				continue
			}

			key, val := parseKeyValue(elem[0].Member.(string))

			mutex.Lock()
			_, err = redisClient.ZRem(key, val).Result()
			mutex.Unlock()
			log.Printf("Removed %s : %s \n", key, val)
			if err != nil {
				// TODO: handle error better
				// E.g. by putting the data back to the sorted set to try to remove it again
				log.Printf("Error removing %s from %s sorted set ... %v\n", val, key, err)
				continue
			}
			invalidExpiryFlag = true

		}
	}
}

func parseKeyValue(fullVall string) (string, string) {
	s := strings.Split(fullVall, ":redismngr:")
	return s[0], s[1]
}

func readConfFile(cfg *redisCreds) {
	f, err := os.Open("config.yml")
	if err != nil {
		return
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		return
	}
}

func readEnv(cfg *redisCreds) {
	err := envconfig.Process("", cfg)
	if err != nil {
		return
	}
}

func handleErr(msg string, err error) {
	if err != nil {
		log.Fatalf("%s ... %v\n", msg, err)
	}
}
