module gitlab.com/ShapedHorizon/redis-set-list-expiration-manager/v2

go 1.14

require (
	github.com/go-redis/redis/v7 v7.2.0 
	github.com/golang/protobuf v1.4.0 
	github.com/kelseyhightower/envconfig v1.4.0 
	google.golang.org/grpc v1.29.0 
	gopkg.in/yaml.v2 v2.2.8 
)
